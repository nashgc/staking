// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./interfaces/irc20.sol";
import "./interfaces/IStaking.sol";

contract Staking is IStaking, Ownable {
    uint256 public rewardPercentBP = 2050;
    uint256 public stakeFreeze = 20 minutes;

    mapping(address => Staker) public stakers;
    struct Staker {
        uint balance;
        uint lastStake;
        uint reward;
    }

    IERC20 public lpToken;
    IERC20 public rewardToken;

    constructor(address _lpToken, address _rewardToken) {
        lpToken = IERC20(_lpToken);
        rewardToken = IERC20(_rewardToken);
    }

    function stake(uint256 _amount) public override {
        require(
            lpToken.allowance(msg.sender, address(this)) >= _amount,
            "You have not allowance to transfer token."
        );
        lpToken.transferFrom(msg.sender, address(this), _amount);
        stakers[msg.sender].balance += _amount;
        stakers[msg.sender].lastStake = block.timestamp;
        stakers[msg.sender].reward += calculateReward(_amount);
        emit Stake(msg.sender, _amount);
    }

    function claim() public override {
        require(stakers[msg.sender].reward > 0, "You have no reward");
        rewardToken.approve(msg.sender, stakers[msg.sender].reward);
        rewardToken.transferFrom(owner(), msg.sender, stakers[msg.sender].reward);
        emit Claim(msg.sender, stakers[msg.sender].reward);
        stakers[msg.sender].reward = 0;
    }

    function unstake() public override {
        require(
            (block.timestamp - stakers[msg.sender].lastStake) >= stakeFreeze,
            "Can't unstake tokens, time is not come."
        );
        lpToken.approve(msg.sender, stakers[msg.sender].balance);
        lpToken.transfer(msg.sender, stakers[msg.sender].balance);
        emit Unstake(msg.sender, stakers[msg.sender].balance);
        stakers[msg.sender].balance = 0;
    }

    function changeRewardPercentBP(uint256 _percent) public override onlyOwner {
        rewardPercentBP = _percent;
    }

    function changeStakeFreeze(uint256 _minutes) public override onlyOwner {
        stakeFreeze = _minutes * 60;
    }

    function calculateReward(uint _amount) private view returns(uint) {
        return _amount * rewardPercentBP / 10000;
    }
}
