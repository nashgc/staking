// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IStaking {

    // Events
    event Stake(address indexed _from, uint256 _value);
    event Claim(address indexed _to, uint256 _value);
    event Unstake(address indexed _to, uint256 _value);

    // Funcs
    function stake(uint256 _amount) external;
    function claim() external;
    function unstake() external;
    function changeRewardPercentBP(uint256 _percent) external;
    function changeStakeFreeze(uint256 _minutes) external;
}