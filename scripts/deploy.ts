import { Staking } from "./../typechain/Staking.d";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";
import { Staking__factory } from "../typechain/factories/Staking__factory";

async function main() {
  const lpToken: string | undefined = process.env.LP_TOKEN_CONTRACT;
  const rewardToken: string | undefined = process.env.REWARD_TOKEN_CONTRACT;

  if (lpToken && rewardToken) {
    const accounts: SignerWithAddress[] = await ethers.getSigners();
    const staking: Staking = await new Staking__factory(accounts[0]).deploy(
      lpToken,
      rewardToken
    );
    console.log("Staking deployed to:", staking.address);
  } else {
    console.log(
      "LP Token address or Reward token address is not provided, please check envs"
    );
  }
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
