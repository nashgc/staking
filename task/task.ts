/* eslint-disable node/no-missing-import */
/* eslint-disable node/no-unpublished-import */
import { task, types } from "hardhat/config";
import { HardhatRuntimeEnvironment } from "hardhat/types";
import { IUniswapV2Router02 } from "../typechain/IUniswapV2Router02";
import { getStakingContract } from "../utils/utils";

import * as dotenv from "dotenv";
const path = require("path");
dotenv.config({ path: path.join(__dirname, "/.env") });

task("createLiquidiryPool", "Create liquidiry pool")
  .addParam(
    "contract",
    "The liquidiry pool contract address",
    process.env.UNISWAP_ROUTER_ADDRESS,
    types.string
  )
  .addParam("token", "A pool token.", undefined, types.string)
  .addParam(
    "amounttokendesired",
    "The amount of token to add as liquidity if the WETH/token price is <= msg.value/amountTokenDesired (token depreciates).",
    undefined,
    types.string
  )
  .addParam(
    "amounttokenmin",
    "Bounds the extent to which the WETH/token price can go up before the transaction reverts. Must be <= amountTokenDesired.",
    undefined,
    types.string
  )
  .addParam(
    "amountethmin",
    "Bounds the extent to which the token/WETH price can go up before the transaction reverts. Must be <= msg.value.",
    undefined,
    types.string
  )
  .addParam("to", "Recipient of the liquidity tokens.", undefined, types.string)
  .addParam(
    "deadline",
    "Add time to current timestamp, after which the transaction will revert.",
    undefined,
    types.int
  )
  .addParam(
    "ether",
    "ether value to send with transaction",
    undefined,
    types.string
  )
  .setAction(
    async (
      taskArgs: {
        contract: string;
        token: string;
        amounttokendesired: string;
        amounttokenmin: string;
        amountethmin: string;
        to: string;
        deadline: number;
        ether: string;
      },
      hre: HardhatRuntimeEnvironment
    ) => {
      const {
        contract,
        token,
        amounttokendesired,
        amounttokenmin,
        amountethmin,
        to,
        deadline,
        ether,
      } = taskArgs;

      const [signer] = await hre.ethers.getSigners();
      console.log(signer.address);

      const router: IUniswapV2Router02 = await hre.ethers.getContractAt(
        "IUniswapV2Router02",
        contract,
        signer
      );

      const deadlineWithExtra = +new Date() + deadline;

      const liquidityResult = await router.addLiquidityETH(
        token,
        amounttokendesired,
        amounttokenmin,
        amountethmin,
        to,
        deadlineWithExtra,
        { value: hre.ethers.utils.parseEther(ether) }
      );
      console.log("Create liquidity pool complete. ", liquidityResult);
    }
  );

task("stake", "Stake some LP token")
  .addParam("contract", "The staking contract address", undefined, types.string)
  .addParam("tokenowner", "The LP token owner address", undefined, types.string)
  .addParam("amount", "Amount LP token for transfer", undefined, types.int)
  .setAction(
    async (
      taskArgs: { contract: string; tokenowner: string; amount: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, tokenowner, amount } = taskArgs;
      const staking = await getStakingContract(hre, contract, tokenowner);
      await staking.stake(amount);
      console.log("Stake is done.");
    }
  );

task("claim", "Claim reward from staking")
  .addParam("contract", "The staking contract address", undefined, types.string)
  .addParam("tokenowner", "The LP token owner address", undefined, types.string)
  .setAction(
    async (
      taskArgs: { contract: string; tokenowner: string },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, tokenowner } = taskArgs;
      const staking = await getStakingContract(hre, contract, tokenowner);
      await staking.claim();
      console.log("Claim is done.");
    }
  );

task("unstake", "Unstake LP tokens from staking")
  .addParam("contract", "The staking contract address", undefined, types.string)
  .addParam("tokenowner", "The LP token owner address", undefined, types.string)
  .setAction(
    async (
      taskArgs: { contract: string; tokenowner: string },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, tokenowner } = taskArgs;
      const staking = await getStakingContract(hre, contract, tokenowner);
      await staking.unstake();
      console.log("Unstake is done.");
    }
  );

task(
  "changerewardpercent",
  "Change reward percent in Base Point (100% = 10000)"
)
  .addParam("contract", "The staking contract address", undefined, types.string)
  .addParam(
    "contractowner",
    "The staking owner address",
    undefined,
    types.string
  )
  .addParam(
    "percent",
    "Percent in Base Point (100% = 10000, so 20% = 2000, etc..)",
    undefined,
    types.int
  )
  .setAction(
    async (
      taskArgs: { contract: string; contractowner: string; percent: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, contractowner, percent } = taskArgs;
      const staking = await getStakingContract(hre, contract, contractowner);
      await staking.changeRewardPercentBP(percent);
      console.log("Change reward percent is done.");
    }
  );

task("changestakefreeze", "Change time before LP tokens can be unstaked")
  .addParam("contract", "The staking contract address", undefined, types.string)
  .addParam(
    "contractowner",
    "The staking owner address",
    undefined,
    types.string
  )
  .addParam("time", "Time in minutes", undefined, types.int)
  .setAction(
    async (
      taskArgs: { contract: string; contractowner: string; time: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, contractowner, time } = taskArgs;
      const staking = await getStakingContract(hre, contract, contractowner);
      await staking.changeStakeFreeze(time);
      console.log("Change freeze time is done.");
    }
  );
