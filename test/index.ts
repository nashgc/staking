import { BigNumber } from "ethers";
/* eslint-disable no-unused-expressions */
import { ERC20__factory } from "./../typechain/factories/ERC20__factory";
import { Staking__factory } from "./../typechain/factories/Staking__factory";
import { expect } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20 } from "../typechain/ERC20";
import { Staking } from "../typechain/Staking";

describe("Staking", () => {
  let owner: SignerWithAddress, acc: SignerWithAddress;
  let rewardToken: ERC20;
  let lpToken: ERC20;
  let staking: Staking;

  beforeEach(async () => {
    [owner, acc] = await ethers.getSigners();

    lpToken = await new ERC20__factory(owner).deploy(
      "Token Uniswap V2",
      "UNI-V2",
      18
    );
    rewardToken = await new ERC20__factory(owner).deploy(
      "Crypton Token",
      "CRY",
      18
    );
    staking = await new Staking__factory(owner).deploy(
      lpToken.address,
      rewardToken.address
    );

    const tokens = ethers.utils.parseUnits("30", 18);
    await lpToken.mint(acc.address, tokens);
    await lpToken.connect(acc).approve(staking.address, tokens);

    await rewardToken.mint(owner.address, tokens);
    await rewardToken.approve(staking.address, tokens);
  });

  it("Should has proper address", async () => {
    expect(rewardToken.address).to.be.properAddress;
    expect(lpToken.address).to.be.properAddress;
    expect(staking.address).to.be.properAddress;
  });

  it("Should deploy token with proper constructor attribute", async function () {
    expect(await staking.lpToken()).to.equal(lpToken.address);
    expect(await staking.rewardToken()).to.equal(rewardToken.address);
  });

  it("Should stake LP tokens", async () => {
    const tokenToStake: BigNumber = ethers.utils.parseUnits("1", 18);
    const rewardPercent: BigNumber = await staking.rewardPercentBP();
    const expectedReward: BigNumber = tokenToStake
      .mul(rewardPercent)
      .div(10000);

    await expect(staking.stake(tokenToStake)).to.be.revertedWith(
      "You have not allowance to transfer token."
    );

    await expect(staking.connect(acc).stake(tokenToStake))
      .to.emit(staking, "Stake")
      .withArgs(acc.address, tokenToStake);

    const blockNumber = await ethers.provider.getBlockNumber();
    const time = await (await ethers.provider.getBlock(blockNumber)).timestamp;

    const stakerInfo = await staking.connect(acc).stakers(acc.address);
    expect(stakerInfo.balance).to.be.equal(tokenToStake);
    expect(stakerInfo.lastStake).to.be.equal(ethers.BigNumber.from(time));
    expect(stakerInfo.reward).to.be.equal(expectedReward);
  });

  it("Should be claim reward", async () => {
    const tokenToStake: BigNumber = ethers.utils.parseUnits("1", 18);
    const rewardPercent: BigNumber = await staking.rewardPercentBP();
    const expectedReward: BigNumber = tokenToStake
      .mul(rewardPercent)
      .div(10000);

    await expect(staking.connect(acc).claim()).to.be.revertedWith(
      "You have no reward"
    );

    await expect(staking.connect(acc).stake(tokenToStake))
      .to.emit(staking, "Stake")
      .withArgs(acc.address, tokenToStake);

    await expect(staking.connect(acc).claim())
      .to.emit(staking, "Claim")
      .withArgs(acc.address, expectedReward);

    expect(await rewardToken.balanceOf(acc.address)).to.be.equal(
      expectedReward
    );
  });

  it("Should unstake LP tokens", async () => {
    const tokenToStake: BigNumber = ethers.utils.parseUnits("1", 18);
    await staking.connect(acc).stake(tokenToStake);

    expect(await lpToken.balanceOf(acc.address)).to.be.equal(
      ethers.utils.parseUnits("29", 18)
    );
    await expect(staking.connect(acc).unstake()).to.be.revertedWith(
      "Can't unstake tokens, time is not come."
    );

    await ethers.provider.send("evm_increaseTime", [1300]);
    await expect(staking.connect(acc).unstake())
      .to.emit(staking, "Unstake")
      .withArgs(acc.address, tokenToStake);

    expect(await lpToken.balanceOf(acc.address)).to.be.equal(
      ethers.utils.parseUnits("30", 18)
    );
  });

  it("Should change reward percentage", async () => {
    expect(await staking.rewardPercentBP()).to.be.equal(2050);
    await staking.changeRewardPercentBP(2560);
    expect(await staking.rewardPercentBP()).to.be.equal(2560);
  });

  it("Should change stake freeze", async () => {
    expect(await staking.stakeFreeze()).to.be.equal(1200);
    await staking.changeStakeFreeze(5);
    expect(await staking.stakeFreeze()).to.be.equal(5 * 60);
  });
});
