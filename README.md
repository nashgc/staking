# Staking contract 

![Coverage](https://camo.githubusercontent.com/2c8b15a3902bc15c0d1e6d70bbf7a1f0f248e2df4b430e25517c7543233530fb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f436f7665726167652d3130302532352d627269676874677265656e2e737667)

## Contract at Rinkeby
[0x453411a4E5D6244603AE75B55732F8A82471a576](
https://rinkeby.etherscan.io/address/0x453411a4E5D6244603AE75B55732F8A82471a576#code)

## Installation & tests

Use npm to install all dependencies

```npm
npm install
```

To check tests coverage
```npm
npx hardhat coverage
```

## Configuration
To work with contract in Rinkeby network, you have to provide api keys in .env file in root project directory.

```npm
ETHERSCAN_API_KEY='Your Etherscan key goes here'
RINKEBY_URL='Your Alchemy rinkeby url goes here'
PRIVATE_KEY='Your Rinkeby API key goes here'
LP_TOKEN_CONTRACT='Your LP tokens contract address goes here'
REWARD_TOKEN_CONTRACT='Your reward tokens contract address goes here'
```

## Usage

```npm
# Deploy contract
npx hardhat --network rinkeby run scripts/deploy.ts

# Verify contract
npx hardhat verify --network rinkeby contract_address lp_token_address reward_token_address

# Stake tokens
npx hardhat --network rinkeby stake --contract contract_address --tokenowner lp_token_owner --amount amount_to_stake

# Stake tokens
npx hardhat --network rinkeby claim --contract contract_address --tokenowner lp_token_owner

# Unstake tokens
npx hardhat --network rinkeby unstake --contract contract_address --tokenowner lp_token_owner

# Change reward percent tokens
npx hardhat --network rinkeby changerewardpercent --contract contract_address --contractowner contract_owner_address --percent percent_amount_in_base_point

# Change time before LP tokens can be unstaked
npx hardhat --network rinkeby changestakefreeze --contract contract_address --contractowner contract_owner_address --time time_in_minutes

```

## Contributing ^_^
Pull requests are welcome. 
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)