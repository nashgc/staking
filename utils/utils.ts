/* eslint-disable node/no-unpublished-import */
/* eslint-disable node/no-missing-import */
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Staking__factory } from "../typechain/factories/Staking__factory";
import { HardhatRuntimeEnvironment } from "hardhat/types";

async function getStakingContract(
  hre: HardhatRuntimeEnvironment,
  contract: string,
  address: string
) {
  const signer: SignerWithAddress = await hre.ethers.getSigner(address);
  return new Staking__factory(signer).attach(contract);
}

export { getStakingContract };
